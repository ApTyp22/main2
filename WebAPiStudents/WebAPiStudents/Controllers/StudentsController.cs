﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WepAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private static readonly string[] Courses = new[]
         {
            "YSU", "ASUE", "NUACA", "ASPU", "YSMU", "NPUA", "YSAFA", "CMSA", "VSMU", "NDRU"
        };
        private static readonly string[] Names = new[]
       {
            "Lernik", "Vardan", "Jivan", "Karen", "Lilit", "Aram", "Karine", "Varduhi", "Amram", "Ani"
        };
        private static readonly string[] LastNames = new[]
      {
            "Lernikyan", "Vardanyan", "Jivanyan", "Karenyan", "Lilityan", "Aramyan", "Karineyan", "Varduhiyan", "Amramyan", "Aniyan"
        };
        private readonly ILogger<StudentsController> _logger;
        public StudentsController(ILogger<StudentsController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public IEnumerable<Students> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Students
            {
                FirstName = Names[rng.Next(Names.Length)],
                LastName = LastNames[rng.Next(LastNames.Length)],
                Age = rng.Next(18, 55),
                Course = Courses[rng.Next(Courses.Length)]
            })
            .ToArray();
        }
    }
}
